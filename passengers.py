import json
from random import randint
import re
import appendjson

regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'


def checkmail(email):
    if (re.search(regex, email)):
        return True
    else:
        return False

def inputnum(text,count):
    while True:
        try:
            question1 = int(input(text))
            if question1 < 1 or question1 > count:
                print("----That's not a valid option!----")
            else:
                return question1
                break
        except:
            print("----That's not a valid option!----")


def register():
    temp={}
    try:
        f = open('passengersslist.json')
        temp = json.load(f)
    except:
        pass
    driver = {}
    username = input("Enter your USERNAME: ").lower()
    if username in temp:
        print ("username already exist. try again")
    else:
        while(True):
            fname = input("Enter your FIRST NAME: ")
            if fname.isalpha():
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        while (True):
            lname = input("Enter your LAST NAME: ")
            if lname.isalpha():
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        while (True):
            phone =  input("Enter your PHONE NUMBER: ")
            if phone.isdigit():
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        while (True):
            email = input("Enter your EMAIL: ")
            if checkmail(email):
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        while (True):
            sex = input("Enter your Sex (m-f): ")
            if sex.lower()=="f" or sex.lower()=="m":
                break
            else:
                print("Not Valid! Try Again... ")
                pass
        password = int(randint(10000, 99999))
        driver[str(username).lower()] = [fname, lname, phone, email, sex, password]
        try:
            appendjson.append_to_json('passengersslist.json',driver)
        except:
            with open('passengersslist.json', 'w') as f:
                json.dump(driver, f)
        print("you have registered successfully! Password: "+str(password))

def login(uname,upass):
    try:
        f = open('passengersslist.json')
        temp = json.load(f)
        uname=str(uname).lower()
    except:
        print("Not Found!")
    else:
        if uname in temp:
            if int(upass) in temp[uname]:
                print ("-----------------------\nFull Name: "+temp[uname][0]+" "+temp[uname][1]+"\nPhone Number: "+temp[uname][2]+"\nEmail: "+temp[uname][3]+"\nSex: "+temp[uname][4]+"\n-----------------------\n1. Request Taxi\n2. Logout")
                x1 = inputnum(": ",2)
                if(x1==1):
                    while(True):
                        x2=input("Enter Destination (Km): ")
                        try:
                            x3=float(x2)
                        except:
                            print("Invalid Number! Try Again")
                            pass
                        else:
                            print("Price: "+str(x3*1000)+" Toman.")
                else:
                    print("bye!")
            else:
                print("Not Found!")
        else:
            print("Not Found!")