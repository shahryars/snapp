import json
def append_to_json(filepath, data):
    new_ending = ", " + json.dumps(data)[1:-1] + "}\n"
    with open(filepath, 'r+') as f:

        f.seek(0, 2)        # move to end of file
        index = f.tell()    # find index of last byte

        while not f.read().startswith('}'):
            index -= 1
            if index == 0:
                raise ValueError("can't find JSON object in {!r}".format(filepath))
            f.seek(index)

        f.seek(index)
        f.write(new_ending)