import json
from random import randint
import re
import appendjson

regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'


def checkmail(email):
    if (re.search(regex, email)):
        return True
    else:
        return False

def register():
    temp={}
    try:
        f = open('driverslist.json')
        temp = json.load(f)
    except:
        pass
    driver = {}
    username = input("Enter your USERNAME: ").lower()
    if username in temp:
        print ("username already exist. try again")
    else:
        while(True):
            fname = input("Enter your FIRST NAME: ")
            if fname.isalpha():
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        while (True):
            lname = input("Enter your LAST NAME: ")
            if lname.isalpha():
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        edu = input("Enter your EDUCATION LEVEL: ")
        car = input("Enter your CAR MODEL: ")
        plaque =  input("Enter your PLAQUE NUMBER: ")
        color = input("Enter your CAR COLOUR: ")
        while (True):
            phone =  input("Enter your PHONE NUMBER: ")
            if phone.isdigit():
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        while (True):
            email = input("Enter your EMAIL: ")
            if checkmail(email):
                break
            else:
                print ("Not Valid! Try Again... ")
                pass
        password = int(randint(10000, 99999))
        driver[str(username).lower()] = [fname, lname, edu, car, plaque, color, phone, email, password]
        try:
            appendjson.append_to_json('driverslist.json',driver)
        except:
            with open('driverslist.json', 'w') as f:
                json.dump(driver, f)
        print("you have registered successfully! Password: "+str(password))

def login(uname,upass):
    try:
        f = open('driverslist.json')
        temp = json.load(f)
        uname = str(uname).lower()
    except:
        print("Not Found!")
    else:
        if uname in temp:
            if int(upass) in temp[uname]:
                print ("-----------------------\nFull Name: "+temp[uname][0]+" "+temp[uname][1]+"\nEducation: "+temp[uname][2]+"\nCar Model: "+temp[uname][3]+"\nPlaque Number: "+temp[uname][4]+"\nCar Colour: "+temp[uname][5]+"\nPhone Number: "+temp[uname][6]+"\nEmail: "+temp[uname][7])
            else:
                print("Not Found!")
        else:
            print("Not Found!")